//
//  CommentCell.h
//  Goddess
//
//  Created by yu_hao on 4/4/14.
//  Copyright (c) 2014 yu_hao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *commentUserNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;

@end
